/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import ejb.CMS_SessionBeanRemote;
import entity.Booking;
import java.io.Serializable;
import java.sql.Date;
import static java.time.Clock.system;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author wongwaiman
 */
@Named(value = "cMS_bean")
@SessionScoped
public class CMS_bean implements Serializable{

    @EJB
    private CMS_SessionBeanRemote cMS_SessionBean;
    private List<Booking> booking_result;
    
    public List<Booking> getBooking_result() {
        return booking_result;
    }

    public void setBooking_result(List<Booking> booking_result) {
        this.booking_result = booking_result;
    }
    //@NotNull(message = "you have to enter your member name for booking")
    private String mem_name,facility;
    //@NotNull(message = "you have to enter your member no for booking")
    private int mem_id,booking_ref;
    //@NotNull(message = "you have to select a date for booking")
    private Date book_date;
    //@NotNull(message = "you have to select a time for booking")
    private String book_start_time,book_end_time;
    //@NotNull(message = "you have to select a time for booking")
    @PersistenceContext(unitName = "CMS_assignmentPU")
    private EntityManager em;
    //@NotNull(message = "you have to select a time for booking")
    @Resource
    private javax.transaction.UserTransaction utx;
    //@NotNull(message = "you have to select a time for booking")
    
    public int getBooking_ref() {
        return booking_ref;
    }

    public void setBooking_ref(int booking_ref) {
        this.booking_ref = booking_ref;
    }
    
    public String getBook_end_time() {
        return book_end_time;
    }

    public void setBook_end_time(String book_end_time) {
        this.book_end_time = book_end_time;
    }
    /**
     * Creates a new instance of CMS_bean
     */
    private String booking_message;

    public String getBooking_message() {
        
        return booking_message;
    }

    public void setBooking_message(String booking_message) {
        this.booking_message = booking_message;
    }
    public CMS_bean() {
        
    }

    public String getMem_name() {
        return mem_name;
    }

    public void setMem_name(String mem_name) {
        this.mem_name = mem_name;
    }

    public String getFacility() {
        return facility;
    }

    public void setFacility(String facility) {
        this.facility = facility;
    }

    public int getMem_id() {
        return mem_id;
    }

    public void setMem_id(int mem_id) {
        this.mem_id = mem_id;
    }

    public Date getBook_date() {
        return book_date;
    }

    public void setBook_date(Date book_date) {
        this.book_date = book_date;
    }

    public String getBook_start_time() {
        return book_start_time;
    }

    public void setBook_start_time(String book_start_time) {
        this.book_start_time = book_start_time;
    }
    
    public void getResult(){
         if (cMS_SessionBean.Result(this.mem_id,this.mem_name,this.facility,this.book_date,this.book_start_time,this.book_end_time)){
            booking_message = "success";
            System.out.println(book_start_time);
         }
    }
    
    public void persist(Object object) {
        try {
            utx.begin();
            em.persist(object);
            utx.commit();
        } catch (Exception e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", e);
            throw new RuntimeException(e);
        }
    }
    
    public void getThebooking(){
        //Query result = em.createNamedQuery("Booking.findByBookingRefandID", Booking.class).setParameter("bookingRef", booking_ref);
        //List booking_list = result.getResultList();
        
        booking_result = cMS_SessionBean.createbooking(this.booking_ref);
        for (int i=0;i<booking_result.size();i++){
            System.out.println(booking_result.get(i));
        }
        
    }
    
}